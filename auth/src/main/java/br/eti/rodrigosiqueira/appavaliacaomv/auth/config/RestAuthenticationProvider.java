package br.eti.rodrigosiqueira.appavaliacaomv.auth.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.netflix.discovery.EurekaClient;

/*
 * Custom provider to authenticate by using endpoints at account service
 */
@Component
public class RestAuthenticationProvider implements AuthenticationManager, AuthenticationProvider {
    
    @Autowired
    private EurekaClient eurekaClient;
    
    public RestAuthenticationProvider() {
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	String username = authentication.getPrincipal().toString();
	String password = authentication.getCredentials().toString();
	// Use eureka to find an available service
	String accountServiceURL = eurekaClient.getNextServerFromEureka("account", false).getHomePageUrl();
	
	Map<String, String> params = new HashMap<>();
	params.put("username", username);
	params.put("password", password);
	
	RestTemplate restTemplate = new RestTemplate();
	
	Boolean authenticUser = restTemplate.postForObject(accountServiceURL + "/authenticate", params, Boolean.class);
	
	if(authenticUser == null || authenticUser == false) {
	    throw new BadCredentialsException("Invalid credentials");
	}
	
	// Grant a simple role for determining a logged user
	List<SimpleGrantedAuthority> grants = new ArrayList<>();
	grants.add(new SimpleGrantedAuthority("USER"));
	
	return new UsernamePasswordAuthenticationToken(username, password, grants);
    }

    @Override
    public boolean supports(Class<?> authentication) {
	return true;
    }

}
