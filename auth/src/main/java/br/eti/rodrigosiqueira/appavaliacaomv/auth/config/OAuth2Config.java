package br.eti.rodrigosiqueira.appavaliacaomv.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {

    /*
     * Use Account service as a provider for checking user credentials
     */
    @Autowired
    private RestAuthenticationProvider restAuthenticationProvider;

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
	oauthServer.tokenKeyAccess("permitAll()")
		.checkTokenAccess("isAuthenticated()");
    }

    /*
     * Configures clients in memory, this should go on a real DB in production.
     */
    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
	clients.inMemory()
		.withClient("web")
		.secret(passwordEncoder().encode("secret"))
		// We should use implicit to redirect user to auth server login page.
		// If application is trusted we could use password grant_type and allow it to authenticate through their login page
		// MUST USE HTTPS
		.authorizedGrantTypes("implicit", "password") 
		.scopes("read", "write")
		.autoApprove(true)
//		.redirectUris("http://localhost/callback")
		.accessTokenValiditySeconds(3600)
		.refreshTokenValiditySeconds(2592000)
		.and()
		.withClient("gateway")
		.secret(passwordEncoder().encode("secret"))
		.authorizedGrantTypes("implicit", "password")
		.scopes("read", "write")
		.accessTokenValiditySeconds(3600)
		.refreshTokenValiditySeconds(2592000);
    }

    /*
     * JWT Configuration
     */
    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
	final DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
	defaultTokenServices.setTokenStore(tokenStore());
	defaultTokenServices.setSupportRefreshToken(true);
	return defaultTokenServices;
    }

    /*
     * Validates user agains Account service through REST
     */
    @Override
    public void configure(final AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
	endpoints.tokenStore(tokenStore())
		.accessTokenConverter(accessTokenConverter())
		.authenticationManager(restAuthenticationProvider);
    }

    @Bean
    public TokenStore tokenStore() {
	return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
	final JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
	converter.setSigningKey("123");
	return converter;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
	return new BCryptPasswordEncoder();
    }
}
