package br.eti.rodrigosiqueira.appavaliacaomv.auth.controller;

import java.security.Principal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    /*
     * Gets current user information.
     */
    @GetMapping("/user/me")
    public Principal user(Principal principal) {
	return principal;
    }
    
    @GetMapping("/health")
    public ResponseEntity<String> healthcheck() {
	return new ResponseEntity<>("ACK", HttpStatus.OK);
    }

}
