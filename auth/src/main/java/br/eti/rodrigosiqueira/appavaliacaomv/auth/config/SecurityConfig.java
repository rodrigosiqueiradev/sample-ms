package br.eti.rodrigosiqueira.appavaliacaomv.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /*
     * Swagger whitelist
     */
    private static final String[] AUTH_WHITELIST = { "/v2/api-docs", "/api/auth/v2/api-docs", "/swagger-resources",
	    "/swagger-resources/**", "/configuration/ui", "/configuration/security", "/swagger-ui.html", "/webjars/**",
	    "/login", "/oauth/token", "/oauth/authorize", "/oauth/check_token" };

    @Autowired
    private RestAuthenticationProvider restAuthenticationProvider;

    /*
     * Uses Account service as authentication method
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	auth.parentAuthenticationManager(restAuthenticationProvider);
    }

    /*
     * Configure security for OAuth server
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
	http.authorizeRequests()
		.antMatchers(AUTH_WHITELIST)
		.permitAll()
		.anyRequest()
		.authenticated()
		.and()
		.formLogin()
		.permitAll()
		.and()
		.logout()
		.clearAuthentication(true)
		.invalidateHttpSession(true)
		.logoutSuccessUrl("/")
		.and()
		.csrf()
		.disable();
    }
}
