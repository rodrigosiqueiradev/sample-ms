import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA
} from '../../../node_modules/@angular/material';
import { Account } from '../account';
import { AccountService } from '../account.service';
import { Subscription } from '../../../node_modules/rxjs';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent implements OnInit, OnDestroy {
  account = new Account();
  isCreate = false;
  accountsSub: Subscription;

  constructor(
    private accountService: AccountService,
    public dialogRef: MatDialogRef<EditAccountComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.account = Object.assign(this.account, this.data.account);
    this.isCreate = this.data.isCreate;
  }

  ngOnDestroy() {
    if (this.accountsSub) {
      this.accountsSub.unsubscribe();
    }
  }

  submit() {
    if (this.isCreate) {
      this.save();
    } else {
      this.update();
    }
  }

  save() {
    this.accountsSub = this.accountService
      .createAccount(this.account)
      .subscribe(
        (data: any) => {
          if (data.account) {
            this.dialogRef.close(data.account);
          } else {
            alert(data.error);
          }
        },
        err => {
          alert(err);
        }
      );
  }

  update() {
    this.accountsSub = this.accountService
      .updateAccount(this.account)
      .subscribe(
        (data: any) => {
          if (data.account) {
            this.dialogRef.close();
          } else {
            alert(data.error);
          }
        },
        err => {
          alert(err);
        }
      );
  }

  reset() {
    this.account = Object.assign(this.account, this.data.account);
  }
}
