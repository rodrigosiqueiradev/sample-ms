import { Component, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '../../node_modules/@angular/router';
import { EventService } from './event.service';
import { MatDialog } from '../../node_modules/@angular/material';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { Account } from './account';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Sample App';
  loaderEnabled = false;

  constructor(
    private dialog: MatDialog,
    public authService: AuthService,
    public router: Router,
    private eventService: EventService
  ) {}

  ngOnInit() {
    this.eventService.loaderStateChanged.subscribe(
      state => (this.loaderEnabled = state)
    );
  }

  ngOnDestroy() {
    this.eventService.loaderStateChanged.unsubscribe();
  }

  login() {
    this.authService.login();
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }

  signup() {
    const dialog = this.dialog.open(EditAccountComponent, {
      height: '450px',
      width: '600px',
      data: {
        account: new Account(),
        isCreate: true
      }
    });

    dialog.afterClosed().subscribe(acc => {
      if (acc.id > 0) {
        alert('Account created, please login.');
      }
    });
  }
}
