import { Injectable, EventEmitter } from '../../node_modules/@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent
} from '../../node_modules/@angular/common/http';
import { Observable } from '../../node_modules/rxjs';
import { finalize } from '../../node_modules/rxjs/operators';
import { EventService } from './event.service';

/*
  Class responsible for appending JWT token to every request automatically
*/
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const idToken = localStorage.getItem('id_token');

    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + idToken)
      });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}

@Injectable()
export class RequestCounter implements HttpInterceptor {
  constructor(private eventService: EventService) {}
  private pendingRequests = 0;
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.pendingRequests++;
    setTimeout(() => {
      this.eventService.loaderStateChanged.emit(true);
    }, 0);

    return next.handle(req).pipe(
      finalize(() => {
        this.pendingRequests--;
        if (this.pendingRequests === 0) {
          setTimeout(() => {
            this.eventService.loaderStateChanged.emit(false);
          }, 500);
        }
      })
    );
  }
}
