import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import * as moment from 'moment';
import * as jwtdecode from 'jwt-decode';
import { Router } from '../../node_modules/@angular/router';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { finalize } from '../../node_modules/rxjs/operators';
import { CookieService } from '../../node_modules/ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: {
    access_token: string;
    expires_in: number;
    jit: string;
    scope: string;
    token_type: string;
  } = {
    access_token: '',
    expires_in: 0,
    jit: '',
    scope: '',
    token_type: ''
  };

  constructor(
    private router: Router,
    private http: HttpClient,
    private cookieService: CookieService
  ) {}

  login() {
    window.location.href = environment.oauth;
  }

  logout() {
    localStorage.clear();
    window.open('http://localhost:9999/auth/logout');
  }

  handleCallback(token: any) {
    const hash = window.location.hash.substring(1);
    if (hash) {
      hash.split('&').map(h => {
        const temp = h.split('=');
        this.auth[temp[0]] = temp[1];
      });

      const expiresAt = moment().add(this.auth.expires_in, 'second');

      setTimeout(() => {
        localStorage.setItem('id_token', this.auth.access_token);
        localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
      });
    }

    this.router.navigateByUrl('/');
  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getLoggedUser() {
    try {
      const obj = jwtdecode(localStorage.getItem('id_token'));
      return obj;
    } catch (ex) {
      return '';
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }
}
