import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Account } from './account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private accountsApi = 'http://localhost:8080/api/account/';

  constructor(private http: HttpClient) {}

  getAccounts() {
    return this.http
      .get<Account[]>(this.accountsApi)
      .pipe(catchError(this.handleError));
  }
  getAccount(id: number) {
    return this.http
      .get<Account>(this.accountsApi + id)
      .pipe(catchError(this.handleError));
  }

  createAccount(account: Account) {
    return this.http
      .post<Account>(this.accountsApi, account)
      .pipe(catchError(this.handleError));
  }

  updateAccount(account: Account) {
    return this.http
      .post<Account>(this.accountsApi + account.id, account)
      .pipe(catchError(this.handleError));
  }

  search(query: string) {
    return this.http
      .get<Account[]>(this.accountsApi + 'search/' + query)
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse | any) {
    console.error('An error occurred', err);
    return throwError(err.message || err);
  }
}
