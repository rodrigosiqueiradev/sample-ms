import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {
  HttpClientModule,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { AccountsComponent } from './accounts/accounts.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { CallbackComponent } from './callback/callback.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { AccountService } from './account.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { FormsModule } from '../../node_modules/@angular/forms';
import { AuthInterceptor, RequestCounter } from './interceptor.module';
import { EventService } from './event.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignupComponent,
    AccountsComponent,
    EditAccountComponent,
    CallbackComponent
  ],
  entryComponents: [EditAccountComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AccountService,
    AuthService,
    AuthGuard,
    CookieService,
    EventService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RequestCounter, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
