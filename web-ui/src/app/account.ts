export class Account {
  id: number;
  username: string;
  name: string;
  email: string;
  password: string;
}
