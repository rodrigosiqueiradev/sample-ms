import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  loaderStateChanged: EventEmitter<boolean> = new EventEmitter();
  constructor() { }
}
