import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { AccountService } from '../account.service';
import { Account } from '../account';
import { Subscription } from '../../../node_modules/rxjs';
import { MatDialog, MatTable } from '../../../node_modules/@angular/material';
import { EditAccountComponent } from '../edit-account/edit-account.component';
import { Router } from '../../../node_modules/@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit, OnDestroy {
  accounts: Account[];
  error: any;
  searchParam: string;
  lastKeypress = 0;

  @ViewChild('table') table: MatTable<Account>;

  accountsSub: Subscription;

  columnsToDisplay = ['id', 'username', 'name', 'email'];

  constructor(
    public accountService: AccountService,
    private dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit() {
    this.search(null);
  }

  ngOnDestroy() {
    this.accountsSub.unsubscribe();
  }

  search($event) {
    if ($event == null || $event.target.value === '') {
      this.accountsSub = this.accountService.getAccounts().subscribe(
        accounts => {
          this.accounts = accounts;
        },
        err => {
          this.error = err;
        }
      );
    } else {
      this.accountsSub = this.accountService
        .search($event.target.value)
        .subscribe(
          accounts => {
            this.accounts = accounts;
          },
          err => {
            this.error = err;
          }
        );
    }
  }

  create() {
    const dialog = this.dialog.open(EditAccountComponent, {
      height: '450px',
      width: '600px',
      data: {
        account: new Account(),
        isCreate: true
      }
    });

    dialog.afterClosed().subscribe(() => {
      this.search(null);
    });
  }

  edit(account: Account) {
    const dialog = this.dialog.open(EditAccountComponent, {
      height: '350px',
      width: '600px',
      data: {
        account: account,
        isCreate: false
      }
    });

    dialog.afterClosed().subscribe(() => {
      this.search(null);
    });
  }
}
