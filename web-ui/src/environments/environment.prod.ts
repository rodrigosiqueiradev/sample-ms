export const environment = {
  production: true,
  oauth:
    'http://localhost:9999/auth/oauth/authorize?client_id=web&response_type=token&redirect_uri=http://localhost/callback',
  gateway: 'http://localhost:8080/'
};
