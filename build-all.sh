#!/bin/bash
echo 'compiling images'
mvn install -f ./auth/pom.xml # > /dev/null 2>&1 &
mvn install -f ./gateway/pom.xml # > /dev/null 2>&1 &
mvn install -f ./account/pom.xml # > /dev/null 2>&1 &
mvn install -f ./registry/pom.xml # > /dev/null 2>&1 &
docker build -t rodrigosdev/web-ui # ./web-ui > /dev/null 2>&1
wait
echo 'done'
