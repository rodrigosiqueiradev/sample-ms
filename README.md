# How to use these files.


## Requirements
 - docker

## Preparations
All required images are published on docker hub. 

## Default credential
User: admin
Pass: admin

## Fire it up
```
$ docker-compose up
```
Web project exposed at [Web UI](http://localhost/)