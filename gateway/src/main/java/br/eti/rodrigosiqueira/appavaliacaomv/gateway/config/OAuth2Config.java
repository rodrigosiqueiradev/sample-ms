package br.eti.rodrigosiqueira.appavaliacaomv.gateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableResourceServer
@EnableWebMvc
public class OAuth2Config extends ResourceServerConfigurerAdapter {
    
    /*
     * Get token validation URI by using properties file,
     * it depends on what profile is being used to allow for Docker DNS resolving.
     */
    @Value("${check-token-uri}")
    private String checkTokenUri;
    
    /*
     * All endpoints needed for swagger
     */
    private static final String[] AUTH_WHITELIST = {
            // -- swagger ui
            "/v2/api-docs",
            "/api/account/v2/api-docs",
            "/api/auth/auth/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
	config.tokenServices(tokenService());
    }

    /*
     * Sets JWT as token provider
     */
    @Bean
    public TokenStore tokenStore() {
	return new JwtTokenStore(accessTokenConverter());
    }

    /*
     * JWT configuration.
     * A simple signing key of 123 was provided, in production this should be a RSA keypair.
     * With public key being provided on open endpoint, and private key kept safe on OAuth server.
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
	JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
	converter.setSigningKey("123");
	return converter;
    }

    /*
     * Makes gateway validate token agains OAuth server.
     */
    @Primary
    @Bean
    public RemoteTokenServices tokenService() {
	RemoteTokenServices tokenService = new RemoteTokenServices();
	tokenService.setCheckTokenEndpointUrl(checkTokenUri);
	tokenService.setClientId("gateway");
	tokenService.setClientSecret("secret");
	return tokenService;
    }

    /*
     * Security rules to enforce validation agains OAuth
     * .antMatchers(HttpMethod.POST, "/api/account/") was allowed to permit user creation
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
	http.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/api/account/")
		.permitAll()
		.antMatchers(AUTH_WHITELIST)
		.permitAll()
		.anyRequest()
		.authenticated()
		.and()
		.httpBasic()
		.disable();
    }

}
