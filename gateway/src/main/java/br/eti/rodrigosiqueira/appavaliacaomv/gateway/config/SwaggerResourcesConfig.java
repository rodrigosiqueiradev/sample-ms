package br.eti.rodrigosiqueira.appavaliacaomv.gateway.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

@Configuration
@Primary
public class SwaggerResourcesConfig implements SwaggerResourcesProvider {
    /*
     * This sets up all intended resources distributed through services
     */
    @Override
    public List<SwaggerResource> get() {
	List<SwaggerResource> resources = new ArrayList<>();
	resources.add(swaggerResource("account", "/api/account/v2/api-docs", "2.0"));
	resources.add(swaggerResource("auth", "/api/auth/auth/v2/api-docs", "2.0"));
	return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
	SwaggerResource swaggerResource = new SwaggerResource();
	swaggerResource.setName(name);
	swaggerResource.setLocation(location);
	swaggerResource.setSwaggerVersion(version);
	return swaggerResource;
    }

}
