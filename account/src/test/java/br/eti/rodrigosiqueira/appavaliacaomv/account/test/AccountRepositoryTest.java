package br.eti.rodrigosiqueira.appavaliacaomv.account.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import br.eti.rodrigosiqueira.appavaliacaomv.account.controller.AccountController;
import br.eti.rodrigosiqueira.appavaliacaomv.account.entity.Account;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountRepositoryTest {

    @Autowired
    private AccountController accountController;

    @Test
    public void test0_save() {
	Account account = new Account();
	account.setName(UUID.randomUUID()
		.toString());
	account.setUsername(UUID.randomUUID()
		.toString());
	account.setEmail(UUID.randomUUID()
		.toString());
	account.setPassword(UUID.randomUUID()
		.toString());

	accountController.save(account);
	account = (Account) accountController.save(account)
		.getBody()
		.get("account");

    }

    @Test
    public void test1_findAll() {
	assertThat(accountController.findAll()
		.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void test2_findOneTest() {
	assertThat(accountController.findOne(1L)
		.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void test3_update() {
	Account account = new Account();
	account.setId(1L);
	account.setName("att");
	account.setUsername("att");
	account.setEmail("att");
	account.setPassword("att");

	account = (Account) accountController.update(account.getId(), account)
		.getBody()
		.get("account");

	assertThat(account.getName()).isEqualTo("att");
	assertThat(account.getUsername()).isEqualTo("att");
	assertThat(account.getEmail()).isEqualTo("att");
	BCryptPasswordEncoder encode = new BCryptPasswordEncoder();

	assertThat(encode.matches("att", account.getPassword()));
    }

    @Test
    public void test4_delete() {
	assertThat(accountController.delete(1L)
		.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

}
