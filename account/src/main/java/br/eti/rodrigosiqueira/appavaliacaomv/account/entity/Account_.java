package br.eti.rodrigosiqueira.appavaliacaomv.account.entity;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/*
 * This class is used by JPA to allow type-safe queries. 
 * This way, we avoid typing mistakes when implementing queries. 
 * If some attribute is wrongly written, compiler will throw an error at compile time
 */
@StaticMetamodel(Account.class)
public class Account_ {
	public static volatile SingularAttribute<Account, Long> id;
	public static volatile SingularAttribute<Account, String> name;
	public static volatile SingularAttribute<Account, String> username;
	public static volatile SingularAttribute<Account, String> email;
	public static volatile SingularAttribute<Account, String> password;
}
