package br.eti.rodrigosiqueira.appavaliacaomv.account.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.eti.rodrigosiqueira.appavaliacaomv.account.dto.ChangePasswordDTO;
import br.eti.rodrigosiqueira.appavaliacaomv.account.entity.Account;
import br.eti.rodrigosiqueira.appavaliacaomv.account.repository.AccountRepository;
import br.eti.rodrigosiqueira.appavaliacaomv.account.specification.AccountSpecification;

/*
 * We could annotate our repository with @RepositoryRestResource and provide a basic RESTful api for CRUD operations.
 * However, we implemented a dedicated controller for handling such operations.
 * This way, we can customize processes and handle more specific scenarios .
 */

@RestController
public class AccountController {

    private final Logger logger;
    private final AccountRepository accountRepository;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private MessageSource messageSource;

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    /*
     * Constructor DI allows for non-modifiable final instance.
     */
    public AccountController(AccountRepository accountRepository) {
	this.logger = LoggerFactory.getLogger(getClass());
	this.accountRepository = accountRepository;
    }

    /*
     * Fetch all records
     */
    @GetMapping("/")
    public ResponseEntity<Iterable<Account>> findAll() {
	logger.info("Fetch all Accounts");
	return new ResponseEntity<>(accountRepository.findAll(), HttpStatus.OK);
    }

    /*
     * Fetch single account, returns NOT_FOUND status if no record with ID exists
     */
    @GetMapping("/{id}")
    public ResponseEntity<Account> findOne(@PathVariable Long id) {
	logger.info("Fetch Account %i", id);
	Account account = accountRepository.findById(id)
		.orElse(null);

	if (account == null) {
	    logger.warn("Attempted to search account with id {}, but it doesn't exists", id);
	    return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	}

	logger.info("Fetched account with id {}", id);
	return new ResponseEntity<Account>(account, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Account> delete(@PathVariable Long id) {
	Account account = accountRepository.findById(id)
		.orElse(null);

	if (account == null) {
	    logger.warn("Attempted to delete account with id:{}, but it doesn't exists", id);
	    return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	}

	accountRepository.delete(account);
	logger.info("Deleted account with id:{}", id);
	return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
     * Saves new account
     */
    @PostMapping("/")
    public ResponseEntity<Map<String, Object>> save(@RequestBody Account account) {
	Map<String, Object> response = new HashMap<>();
	List<String> errors = new ArrayList<>();

	/*
	 * Perform general validation before beginning
	 */
	if (account.getEmail() == null || account.getEmail()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.email", null, request.getLocale()));
	}

	if (account.getName() == null || account.getName()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.name", null, request.getLocale()));
	}

	if (account.getUsername() == null || account.getUsername()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.username", null, request.getLocale()));
	}

	if (account.getPassword() == null || account.getPassword()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.password", null, request.getLocale()));
	}

	Specification<Account> hasUsername = AccountSpecification.hasUsername(account.getUsername());
	Specification<Account> hasEmail = AccountSpecification.hasEmail(account.getEmail());

	if (accountRepository.findOne(Specification.where(hasUsername)
		.or(hasEmail))
		.isPresent()) {
	    errors.add(messageSource.getMessage("error.username.email.exists", null, request.getLocale()));
	}

	if (!errors.isEmpty()) {
	    response.put("error", errors);
	    logger.warn("Trying to save new account: missing fields or already exists");
	    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	/*
	 * Encrypt password before saving to DB
	 */
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	account.setPassword(encoder.encode(account.getPassword()));

	/*
	 * In case an id was provided by the client. Setting it to null will prevent JPA
	 * from updating another record.
	 */
	account.setId(null);

	logger.debug("Account created: ", account.getName());
	account = accountRepository.save(account);

	response.put("account", account);

	return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /*
     * Updates account
     */
    @PostMapping("/{id}")
    public ResponseEntity<Map<String, Object>> update(@PathVariable Long id, @RequestBody Account account) {
	Map<String, Object> response = new HashMap<>();
	List<String> errors = new ArrayList<>();

	/*
	 * Perform general validation before beginning
	 */
	Account oldAccount = accountRepository.findById(id)
		.orElse(null);

	if (oldAccount == null) {
	    errors.add(messageSource.getMessage("error.account.not.exists", null, request.getLocale()));
	}

	if (account.getEmail() == null || account.getEmail()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.email", null, request.getLocale()));
	}

	if (account.getName() == null || account.getName()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.name", null, request.getLocale()));
	}

	Specification<Account> spec = null;
	// Email is unique so validation if new email doesn't currently exists is
	// necessary
	if (!oldAccount.getEmail()
		.equals(account.getEmail())) {
	    spec = Specification.where(AccountSpecification.hasEmail(account.getEmail()));
	}

	if (spec != null && !accountRepository.findAll(spec)
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.username.email.exists", null, request.getLocale()));
	}

	if (!errors.isEmpty()) {
	    response.put("error", errors);
	    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	// Password changing will be held on another method to better separate
	// responsibility and validation
	account.setPassword(oldAccount.getPassword());
	account = accountRepository.save(account);
	response.put("account", account);
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/password")
    public ResponseEntity<Map<String, Object>> changePassword(@RequestBody ChangePasswordDTO changePasswordDTO) {
	Map<String, Object> response = new HashMap<>();
	List<String> errors = new ArrayList<>();
	if (changePasswordDTO.getUsername() == null || changePasswordDTO.getUsername()
		.trim()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.username", null, request.getLocale()));
	}

	if (changePasswordDTO.getOldPassword() == null || changePasswordDTO.getOldPassword()
		.trim()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.password", null, request.getLocale()));
	}

	if (changePasswordDTO.getNewPassword() == null || changePasswordDTO.getNewPassword()
		.trim()
		.isEmpty()) {
	    errors.add(messageSource.getMessage("error.empty.password", null, request.getLocale()));
	}

	if (!errors.isEmpty()) {
	    response.put("error", errors);
	    return new ResponseEntity<>(response, HttpStatus.OK);
	}
	/*
	 * Validate that an Account with provided username and password exists
	 */
	Account account = accountRepository
		.findOne(Specification.where(AccountSpecification.hasUsername(changePasswordDTO.getUsername())
			.and(AccountSpecification.hasPassword(changePasswordDTO.getOldPassword()))))
		.orElse(null);

	if (account == null) {
	    logger.debug("Wrong old password when changing current password");
	    errors.add(messageSource.getMessage("error.wrong.password", null, request.getLocale()));
	    response.put("error", errors);
	    return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/*
	 * Encrypt new password
	 */
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	account.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
	account = accountRepository.save(account);
	logger.debug("Updated account: {}", account.getId());
	response.put("account", account);
	return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /*
     * Responsible for checking if credentials are valid. This will be used by SSO
     * Service for credentials
     */
    @PostMapping("/authenticate")
    public ResponseEntity<Boolean> authenticate(@RequestBody Map<String, String> body) {
	String username = body.get("username");
	String password = body.get("password");

	Optional<Account> account = accountRepository
		.findOne(Specification.where(AccountSpecification.hasUsername(username)));
	if (account.isPresent()) {
	    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	    return new ResponseEntity<>(passwordEncoder.matches(password, account.get()
		    .getPassword()), HttpStatus.OK);
	}
	return new ResponseEntity<>(false, HttpStatus.OK);
    }

    @GetMapping("/search/{query}")
    public ResponseEntity<Iterable<Account>> search(@PathVariable String query) {
	List<Account> accounts = accountRepository.findAll(Specification.where(AccountSpecification.hasUsernameNoCase(query)
			.or(AccountSpecification.hasEmailNoCase(query))
			.or(AccountSpecification.hasNameNoCase(query))));
	
	return new ResponseEntity<>(accounts, HttpStatus.OK);

    }

}
