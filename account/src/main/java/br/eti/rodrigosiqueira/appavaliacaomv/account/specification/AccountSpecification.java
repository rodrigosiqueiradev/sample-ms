package br.eti.rodrigosiqueira.appavaliacaomv.account.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.eti.rodrigosiqueira.appavaliacaomv.account.entity.Account;
import br.eti.rodrigosiqueira.appavaliacaomv.account.entity.Account_;

public final class AccountSpecification {

	/*
	 * This class works as a builder for specifications. So instantiation should not be possible.
	 */
	private AccountSpecification() {}

	public static Specification<Account> hasPassword(String password) {
	    return (root, query, cb) -> {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return cb.isTrue(cb.literal(passwordEncoder.matches(password, root.get(Account_.password).toString())));
	    };
	}
	
	public static Specification<Account> hasId(Long id) {
		return (root, query, cb) -> {
			return cb.equal(root.get(Account_.id), id);
		};
	}

	public static Specification<Account> hasUsername(String username) {
		return (root, query, cb) -> {
			return cb.equal(root.get(Account_.username), username);
		};
	}
	
	public static Specification<Account> hasEmail(String email) {
		return (root, query, cb) -> {
			return cb.equal(root.get(Account_.email), email);
		};
	}
	
	public static Specification<Account> hasName(String name) {
		return (root, query, cb) -> {
			return cb.equal(root.get(Account_.name), name);
		};
	}
	
	public static Specification<Account> hasUsernameNoCase(String username) {
		return (root, query, cb) -> {
			return cb.like(cb.lower(root.get(Account_.username)), "%" + username.toLowerCase() + "%");
		};
	}
	
	public static Specification<Account> hasEmailNoCase(String email) {
		return (root, query, cb) -> {
			return cb.like(cb.lower(root.get(Account_.email)), "%" + email.toLowerCase() + "%");
		};
	}
	
	public static Specification<Account> hasNameNoCase(String name) {
		return (root, query, cb) -> {
			return cb.like(cb.lower(root.get(Account_.name)), "%" + name.toLowerCase() + "%");
		};
	}
	
}
