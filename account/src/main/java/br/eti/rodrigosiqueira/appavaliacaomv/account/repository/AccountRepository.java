package br.eti.rodrigosiqueira.appavaliacaomv.account.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.eti.rodrigosiqueira.appavaliacaomv.account.entity.Account;

/*
 * Default methods extended by: 
 * CrudRepository<Account, Long>, JpaSpecificationExecutor<Account>
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long>, JpaSpecificationExecutor<Account>{
	
}
