package br.eti.rodrigosiqueira.appavaliacaomv.account.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Account {
	/*
	 * Let's configure id as PK of our entity and make JPA generate it's value
	 * automatically
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/*
	 * Our user's name can't be null or empty
	 */
	@NotBlank
	private String name;

	/*
	 * The username (aka. login) should be set as a UNIQUE value on our table and
	 * can't be null or empty
	 */
	@NotBlank
	@Column(unique = true)
	private String username;

	/*
	 * The email should obey the same rule as username
	 */
	@NotBlank
	@Column(unique = true)
	private String email;

	/*
	 * Password will be stored using BCrypt hashing algorithm Here we added
	 * JsonIgnore so this field will be ignored when parsing to JSON
	 */
	@NotBlank
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
