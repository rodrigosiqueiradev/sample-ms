CREATE TABLE account (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  email varchar(255) NOT NULL,
  name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  PRIMARY KEY PK_ACCOUNT (id),
  UNIQUE KEY UK_ACCOUNT_EMAIL (email),
  UNIQUE KEY UK_ACCOUNT_USERNAME (username)
);